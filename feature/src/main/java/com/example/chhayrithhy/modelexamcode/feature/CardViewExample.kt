package com.example.chhayrithhy.modelexamcode.feature

import android.graphics.Typeface
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.res.ResourcesCompat
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView

class CardViewExample : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_card_view_example)

        val font: Typeface? = ResourcesCompat.getFont(this, R.font.one_time)
        val nextBtn = findViewById<Button>(R.id.nextBtn)
        nextBtn.typeface = font

        val title = findViewById<TextView>(R.id.title)
        title.typeface = font

        val acti = findViewById<TextView>(R.id.activity4)
        acti.typeface = font

        var count = 1;
        var fishList = arrayListOf(
                arrayListOf(R.drawable.fish_1, "Fish 1"),
                arrayListOf(R.drawable.fish_2, "Fish 2"),
                arrayListOf(R.drawable.fish_3, "Fish 3"),
                arrayListOf(R.drawable.fish_4, "Fish 4")
        )

        val image = findViewById<ImageView>(R.id.image)

        nextBtn.setOnClickListener(){
            image.setImageResource(fishList.get(count).get(0) as Int)
            title.text = fishList.get(count).get(1) as String

            count += 1
        }

    }
}
